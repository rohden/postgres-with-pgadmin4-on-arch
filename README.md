# Postgres Setup

Package install: 
```
sudo pacman -S postgresql
```

Create data folder:
```
mkd -p /media/storage/postgres/data
sudo chown -R postgres:postgres /media/storage/postgres/data
```

Switch to pg user and setup the db
```
sudo -iu postgres
initdb -D /media/storage/postgres/data --locale=en_US.UTF-8 -E UTF8
exit
```

There is no need to run the command that `initdb` outputs at the end (`pg_ctl ...`).

Since `postgres/data` is not in `/var/lib` we have to update the service:
```
sudo systemctl edit postgresql
```

Put this at the top of the file (above the comments, or it will get deleted):
```
[Service]
Environment=PGROOT=/media/storage/postgres
```

Then start service:
```
sudo systemctl enable --now postgresql
```

Setting up the user and db:
```
sudo -iu postgres
createuser -P --interactive
createdb <dbname>
```


# PgAdmin4 Setup

Install:
```
sudo pacman -S pgadmin4
```

Run: 
```
pgadmin4
```



